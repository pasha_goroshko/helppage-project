﻿using HelpPage.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace HelpPage.Actions
{
    public class XMLtoCSHARP
    {
        public static Doc ParseDocument(string path)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException(path);

            XmlSerializer serialize = new XmlSerializer(typeof(Doc));
            using (var fs = new FileStream(path, FileMode.Open))
            {
                XmlReader reader = XmlReader.Create(fs);
                return (Doc)serialize.Deserialize(reader);
            }
        }
    }
}