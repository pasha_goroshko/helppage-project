﻿using HelpPage.Actions;
using HelpPage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace HelpPage.Controllers
{
    public class HomeController : Controller
    {
        string path = @"C:\development\customerpricing\CustomerPricing.Remoting\App_Data\CustomerPricing.Remoting.XML";
        // GET: Home
        public ActionResult Index(string g)
        {
            ViewData.Model = GetComments();

            return View();
        }

        public ActionResult Edit(Method item)
        {
            ViewData.Model = GetComments().Where(x => x.Name == item.Name).FirstOrDefault();
            return View();
        }

        
        private MethodInfo[] GetMethodsFromContract(Type t1)
        {
            return t1.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
        }

        private List<Method> GetComments()
        {
            MethodInfo[] methods = GetMethodsFromContract(typeof(CustomerPricing.Remoting.ICustomerPricingServiceContract));
            Doc doc = XMLtoCSHARP.ParseDocument(path);

            List<Method> newMethods = new List<Method>();
            foreach (var method in methods)
            {
                var paramms = method.GetParameters();
                var t = doc.Members.Member.Where(x =>
                {
                    var index = x.Name.IndexOf('(') == -1 ? x.Name.Length : x.Name.IndexOf('(');
                    var count = x.Name.Length - index;
                    var d = x.Name.Remove(index, count);
                    var fdf = d.Split('.')[d.Split('.').Length - 1];
                    return d.Split('.')[d.Split('.').Length - 1] == method.Name;
                }).FirstOrDefault();

                newMethods.Add(new Method
                {
                    Name = method.Name,
                    Description = t?.Summary,
                    Returned = new Returned
                    {
                        Name = method.ReturnType.Name + "!@!@" + method.ReturnType.GenericTypeArguments.FirstOrDefault()?.Name,
                        Description = t?.Returns,
                        Type = method.ReturnType.Name
                    },
                    Parametrs = paramms.Select(x =>
                    {
                        return new Parametr
                        {
                            Name = x.Name,
                            Description = t?.Param.Where(y=>y.Name == x.Name).FirstOrDefault()?.Text,
                            Type = x.ParameterType
                        };

                    }).ToList()
                });
            }
            return newMethods;
        }
    }

    public class Doc1
    {
        public string Name { get; set; }
        public IEnumerable<Method> Methods { get; set; }
    }

    public class Method
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Returned Returned { get; set; }
        public IEnumerable<Parametr> Parametrs { get; set; }
    }
    public class Returned
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
    }
    public class Parametr
    {
        public string Name { get; set; }
        public Type Type { get; set; }
        public string Description { get; set; }
    }
}